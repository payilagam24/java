public class BookShop1
{
	int num;
	String str;
public BookShop1(int price)
{
	System.out.println("one args constructor");
}
public BookShop1()
{
	System.out.println("no args constructor");
}
public BookShop1(int no,double no2)
{
	System.out.println("two args constructor");
}	
public static void main(String[] args)
{
	BookShop1 book1 = new BookShop1();
	BookShop1 book2 = new BookShop1(35);
	BookShop1 book3 = new BookShop1(35,45.5);
	BookShop1 book4 = new BookShop1();
	BookShop1.add();
}
public static void add()
{
	System.out.println("static method");
}
	
}